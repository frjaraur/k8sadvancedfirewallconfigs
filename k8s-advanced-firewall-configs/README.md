Role Name
=========

Kubernetes Advanced Firewalld Configurations using zones.

Requirements
------------

This Role should be applied to already created clusters or during their creaion. As different configurations will be applied to Control Plane and Worker nodes, it is mandatory to add a "role" variable to all cluster nodes. This variable should be a list of all roles applied to each node. From this list of roles, only "controlplane" and "worker" will be used in this ansible role. For example:

```
  hosts:
    master1:
      ansible_host: 192.168.202.131
      nodename: master1
      roles: [controlplane, leader] <-- In this case 'leader' is also applied as it is required for our this-project´s-external cluster creation procedure, but feel free to add whatever list of roles required to your environment.
    worker1:
      ansible_host: 192.168.202.134
      nodename: worker1
      roles: [worker]
```


Role Variables
--------------
In this Ansible Role, all variables will be used to describe Firewalld behavior for our Kubernetes deployment:

fwZones - Describes the zones that should be created and their default target. It's also mandatory to include which node´s interfaces will be applied to this zone.

fwKubernetesCIDRs - Describes which CIDRs should be trusted to avoid dropping of Kubernetes internal traffic.

fwCommonServices and fwCommonPorts - Describes services and ports that should be applied to all hosts in specified zone. Multiple entries can be used for same service on different zones.

fwControlPlanePorts and fwWorkloadPorts -  Describes ports that should be applied to hosts with "controlplane" and "worker" (workload nodes) roles, respectively, in specified zone. Multiple entries can be used for same service on different zones in case you need to allow this port for a role with ingress traffic from different interfaces.


Dependencies
------------

Does not have any other role dependency. Firewalld software will be installed.

Example Playbook
----------------

Let´s take following inventory as example:
```
all:
  hosts:
    master1:
      ansible_host: 192.168.202.131
      nodename: master1
      roles: [controlplane, leader]
    worker1:
      ansible_host: 192.168.202.134
      nodename: worker1
      roles: [worker]
    worker2:
      ansible_host: 192.168.202.135
      nodename: worker2
      roles: [worker]
  children:
    leader:
      hosts:
        master1:
    controlplane:
      hosts:
        master1:
    workers:
      hosts:
        worker1:
        worker2:
```

Following, there are some example variables for cluster with master1 controlplane role and 2 worker nodes. Two zones are defined to allow ingress traffic only from interface eth1, while ssh is permited from both interfaces (eth0 and eth1). Internal Kubernetes traffic is applied to eth0 interface only:
```
fwZones:
- name: kubernetes
  target: DROP
  interfaces:
  - eth0

- name: ingress
  target: DROP
  interfaces:
  - eth1

fwKubernetesCIDRs:
- CIDR: "10.42.0.0/16"
  scope: "pods"
  zone: "trusted"
- CIDR: "10.43.0.0/16"
  scope: "services"
  zone: "trusted"

fwCommonServices:
- name: ssh
  port: 22
  proto: "tcp"
  comment: "SSH Access"
  zone: kubernetes

- name: ssh
  port: 22
  proto: "tcp"
  comment: "SSH Access"
  zone: ingress

fwCommonPorts: []

fwControlPlanePorts:
- name: Kube-APIServer
  port: 6443
  proto: "tcp"
  comment: "Kube APIServer"
  zone: kubernetes

- name: Kubelet
  port: 10250
  proto: "tcp"
  comment: "Kubelet"
  zone: kubernetes
  
fwWorkloadPorts:
- name: Kubelet
  port: 10250
  proto: "tcp"
  comment: "Kubelet"
  zone: kubernetes

- name: Node Port Range
  port: "30000-32767"
  proto: "tcp"
  comment: "NodePort Port Range"
  zone: kubernetes

- name: Ingress HTTP
  port: 80
  proto: "tcp"
  comment: "Kubernetes Ingress HTTP"
  zone: ingress

- name: Ingress HTTPS
  port: 443
  proto: "tcp"
  comment: "Kubernetes Ingress HTTPS"
  zone: ingress
```

With these variables, role will be used in following playbook:

```
- name: Advacned Firewalld Rules for Kubernetes
  hosts: all
  become: true
  gather_facts: yes
  roles:
  - role: k8s-advanced-firewall-configs
```


License
-------

BSD

Author Information
------------------

frjaraur - SecDevOps